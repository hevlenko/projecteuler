#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Число-палиндром с обеих сторон (справа налево и слева направо) читается одинаково. 
Самое большое число-палиндром, полученное умножением двух двузначных чисел – 9009 = 91 × 99.
Найдите самый большой палиндром, полученный умножением двух трехзначных чисел.
'''

import time

def is_polindrom(num):
    num_inverted = int(str(num)[::-1])
    if num == num_inverted:
        return True
    else:
        return False


start = time.process_time()

max_polindrom = 0
for i in range(100, 1000):
    for j in range(100, 1000):
        tmp = i * j
        if is_polindrom(tmp):
            if max_polindrom < tmp:
                max_polindrom = tmp
print(max_polindrom)

print('time %f' %(time.process_time() - start))
