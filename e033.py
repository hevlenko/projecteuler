#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Дробь 49/98 является любопытной, поскольку неопытный математик, 
пытаясь сократить ее, будет ошибочно полагать, что 49/98 = 4/8, 
являющееся истиной, получено вычеркиванием девяток.

Дроби вида 30/50 = 3/5 будем считать тривиальными примерами.

Существует ровно 4 нетривиальных примера дробей подобного типа, 
которые меньше единицы и содержат двухзначные числа как в числителе, так и в знаменателе.

Пусть произведение этих четырех дробей дано в виде несократимой дроби 
(числитель и знаменатель дроби не имеют общих сомножителей). Найдите знаменатель этой дроби.
'''

import time
# import math

start = time.process_time()

# a/b
ab = []
for a in range(10, 100):
    for b in range(a+1, 100):
        # print(a,b)
        if str(a)[0]==str(b)[0] and int(str(a)[1]*b==int(str(b)[1])*a):
            ab.append((a,b))
        elif str(a)[1]==str(b)[1] and str(a)[1]!='0' and int(str(a)[0])*b==int(str(b)[0])*a:
            ab.append((a,b))
        elif str(a)[0]==str(b)[1] and int(str(a)[1])*b==int(str(b)[0])*a:
            ab.append((a,b))
        elif str(a)[1]==str(b)[0] and int(str(a)[0])*b==int(str(b)[1])*a:
            ab.append((a,b))

a=1
b=1
for x in ab:
    a *= x[0]
    b *= x[1]
i = 2
while i <= a:
    if a%i == 0 and b%i == 0:
        a /= i
        b /= i
        i = 1
    i += 1

print(int(b))



print('time %f' %(time.process_time() - start))
