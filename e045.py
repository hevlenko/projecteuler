#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Треугольные, пятиугольные и шестиугольные числа вычисляются по нижеследующим формулам:

Треугольные     Tn=n(n+1)/2     1, 3, 6, 10, 15, ...
Пятиугольные        Pn=n(3n−1)/2        1, 5, 12, 22, 35, ...
Шестиугольные       Hn=n(2n−1)      1, 6, 15, 28, 45, ...
Можно убедиться в том, что T285 = P165 = H143 = 40755.

Найдите следующее треугольное число, являющееся также пятиугольным и шестиугольным.
'''

import time
# import math

start = time.process_time()

# def t(n):
#     return int(n*(n+1)/2)
def p(n):
    return int(n*(3*n-1)/2)
# def h(n):
#     return int(n*(2*n-1))

# def is3(n):
#     return True if ((1+8*n)**0.5 - 1.0)%2.0 == 0 else False
def is5(n):
    return True if ((1+24*n)**0.5 + 1.0)%6.0 == 0 else False
def is6(n):
    return True if ((1+8*n)**0.5 + 1.0)%4.0 == 0 else False

notFound = True
i = 285
# j = 165
# k = 143
while notFound:
    i += 1
    num = p(i)
    if is5(num) and is6(num):
        notFound = False
        break

print('N=', num)


print('time %f' %(time.process_time() - start))