#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Начиная в вершине треугольника (см. пример ниже) и перемещаясь вниз на смежные числа, 
максимальная сумма до основания составляет 23.

   3
  7 4
 2 4 6
8 5 9 3

То есть, 3 + 7 + 4 + 9 = 23.

Найдите максимальную сумму пути от вершины до основания следующего треугольника:

75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23



Примечание: Так как в данном треугольнике всего 16384 возможных маршрута 
от вершины до основания, эту задачу можно решить проверяя каждый из маршрутов. 
Однако похожая Задача 67 с треугольником, состоящим из сотни строк, не решается 
перебором (brute force) и требует более умного подхода! ;o)
'''

import time

def f_read(file_name='e018.txt'):
    base = []
    with open(file_name) as file:
        for line in file.readlines():
            line_x = line.split()
            base.append(line_x)
            for i, item in enumerate(base[-1]):
                base[-1][i] = int(item)
    return base

def step(base):
    while len(base[-1]) > 1:
        for i, item in enumerate(base[-2]):
            if item + base[-1][i] > item + base[-1][i+1]:
                base[-2][i] += base[-1][i]
            else:
                base[-2][i] += base[-1][i+1]
        base.pop()
    return base
    

start = time.process_time()

base = f_read()

step(base)
print(base)

print('time %f' %(time.process_time() - start))
