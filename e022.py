#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Используйте names.txt (щелкнуть правой кнопкой мыши и выбрать 'Save Link/Target As...'), 
текстовый файл размером 46 КБ, содержащий более пяти тысяч имен. 
Начните с сортировки в алфавитном порядке. Затем подсчитайте алфавитные значения каждого имени 
и умножьте это значение на порядковый номер имени в отсортированном списке для получения количества очков имени.

Например, если список отсортирован по алфавиту, имя COLIN 
(алфавитное значение которого 3 + 15 + 12 + 9 + 14 = 53) является 938-ым в списке. 
Поэтому, имя COLIN получает 938 × 53 = 49714 очков.

Какова сумма очков имен в файле?
'''

import time
from string import ascii_uppercase as alphabet
    

start = time.process_time()


char2num = {letter: alphabet.index(letter) + 1 for letter in alphabet}
data=open("p022_names.txt",'r')
names=sorted(data.read().replace('"','').split(','),key=str)
sum=0
for i, val in enumerate(names):
    temp = 0
    for x in val:
        temp += char2num[x]
    sum += temp * (i + 1)
    
print(sum)

print('time %f' %(time.process_time() - start))

