#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Число 1406357289, является пан-цифровым, поскольку оно состоит из цифр от 0 до 9 в определенном порядке. Помимо этого, оно также обладает интересным свойством делимости подстрок.

Пусть d1 будет 1-ой цифрой, d2 будет 2-ой цифрой, и т.д. В таком случае, можно заметить следующее:

d2d3d4=406 делится на 2 без остатка
d3d4d5=063 делится на 3 без остатка
d4d5d6=635 делится на 5 без остатка
d5d6d7=357 делится на 7 без остатка
d6d7d8=572 делится на 11 без остатка
d7d8d9=728 делится на 13 без остатка
d8d9d10=289 делится на 17 без остатка
Найдите сумму всех пан-цифровых чисел из цифр от 0 до 9, обладающих данным свойством.
'''
import time


start = time.process_time()

def o(n):
    d = str(n)
    if int(d[1:4])%2==0 and \
        int(d[2:5])%3==0 and \
        int(d[3:6])%5==0 and \
        int(d[4:7])%7==0 and \
        int(d[5:8])%11==0 and \
        int(d[6:9])%13== 0 and \
        int(d[7:10])%17==0:
        return True
    else:
        return False
s = 0

digitals = '1234567890'
d_1 = digitals.replace('0', '')
for d1 in d_1:
    d_2 = digitals.replace(d1, '')
    for d2 in d_2:
        d_3 = d_2.replace(d2, '')
        for d3 in d_3:
            d_4 = d_3.replace(d3, '')
            for d4 in d_4:
                d_5 = d_4.replace(d4, '')
                for d5 in d_5:
                    d_6 = d_5.replace(d5, '')
                    for d6 in d_6:
                        d_7 = d_6.replace(d6, '')
                        for d7 in d_7:
                            d_8 = d_7.replace(d7, '')
                            for d8 in d_8:
                                d_9 = d_8.replace(d8, '')
                                for d9 in d_9:
                                    d10 = d_9.replace(d9, '')
                                    n = int(d1+d2+d3+d4+d5+d6+d7+d8+d9+d10)
                                    # print(n)
                                    if o(n):
                                        s += n


print(s)

print('time %f' %(time.process_time() - start))