#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
n! означает n × (n − 1) × ... × 3 × 2 × 1

Например, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
и сумма цифр в числе 10! равна 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Найдите сумму цифр в числе 100!.
'''

import time

start = time.process_time()

def fact(n):
    res = 1
    for i in range(1, n+1):
        res *= i
    return res


n = 100
s = 0
for i in str(fact(n)):
    s += int(i)

print(s)


print('time %f' %(time.process_time() - start))