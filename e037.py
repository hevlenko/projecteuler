#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Число 3797 обладает интересным свойством. Будучи само по себе простым числом, 
из него можно последовательно выбрасывать цифры слева направо, 
число же при этом остается простым на каждом этапе: 3797, 797, 97, 7. 
Точно таким же способом можно выбрасывать цифры справа налево: 3797, 379, 37, 3.

Найдите сумму единственных одиннадцати простых чисел, 
из которых можно выбрасывать цифры как справа налево, так и слева направо, 
но числа при этом остаются простыми.

ПРИМЕЧАНИЕ: числа 2, 3, 5 и 7 таковыми не считаются.
'''

import time

start = time.process_time()

def is_prime(n):
    if n < 2: 
        return False;
    if n % 2 == 0:             
        return n == 2
    k = 3
    while k*k <= n:
        if n % k == 0:
            return False
        k += 2
    return True

def o(n):
    tmp = str(n)
    res = True
    i = 1
    while i < len(str(n)):
        if is_prime(int(tmp[i:])) and  is_prime(int(tmp[:-i])):
            pass
        else:
            res = False
            i = len(str(n))
        i += 1
    return res

x = []
i = 7
while len(x) < 11:
    i += 2
    if is_prime(i) and o(i):
        x.append(i)

print(sum(x), ':', x)


print('time %f' %(time.process_time() - start))