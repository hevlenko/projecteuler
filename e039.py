#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Если p - периметр прямоугольного треугольника с целочисленными длинами сторон {a,b,c}, 
то существует ровно три решения для p = 120:

{20,48,52}, {24,45,51}, {30,40,50}

Какое значение p ≤ 1000 дает максимальное число решений?
'''

import time


start = time.process_time()

p = 1001
res = [0] * (p)
for a in range(1, p):
    for b in range(a, p):
        c = int((a**2 + b**2)**(1/2))
        if c**2 == a**2 + b**2 and a + b + c <= 1000:
            res[a+b+c] += 1



print(res.index(max(res)))

print('time %f' %(time.process_time() - start))