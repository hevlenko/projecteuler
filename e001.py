#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Если выписать все натуральные числа меньше 10, кратные 3 или 5, то получим 3, 5, 6 и 9. Сумма этих чисел равна 23.
Найдите сумму всех чисел меньше 1000, кратных 3 или 5.
'''
import time

max = 1000

start = time.process_time()
result = 0
for n in range(max):
    if n % 3 == 0 or n % 5 == 0:
        result += n
print(result)
print('time %f' %(time.process_time() - start))


start = time.process_time()
print(sum(i for i in range(max) if i % 5 == 0 or i % 3 == 0))
print('time %f' %(time.process_time() - start))


start = time.process_time()
n3= int(max/3)
n5 = int(max/5)
n15 = int(max/15)
sum = 3 * (n3 * (n3 + 1) / 2) + 5 * (n5 * (n5 + 1) / 2) - 15 * (n15 * (n15 + 1) / 2)
print(int(sum))
print('time %f' %(time.process_time() - start))