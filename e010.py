#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Сумма простых чисел меньше 10 равна 2 + 3 + 5 + 7 = 17.

Найдите сумму всех простых чисел меньше двух миллионов.
'''

import time
import math

N = 2000000

def is_prime(n, l):
    for i in range(2, math.ceil(math.sqrt(l[-1]))+1):
        if n % i == 0:
            return False
    return True

start = time.process_time()

prime = [2, 3]
tmp = prime[-1]
no_stop = True
while no_stop:
    tmp += 2
    if tmp < N:
        if is_prime(tmp, prime):
            prime.append(tmp)
    else:
        no_stop = False
print(sum(prime))

print('time %f' %(time.process_time() - start))
