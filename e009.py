#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Тройка Пифагора - три натуральных числа a < b < c, для которых выполняется равенство

a2 + b2 = c2
Например, 32 + 42 = 9 + 16 = 25 = 52.

Существует только одна тройка Пифагора, для которой a + b + c = 1000.
Найдите произведение abc.
'''


'''
Первые формулы зависимостей троек Пифагора известны из трудов Эвклида 2100 г до н.э, самая известная из них:
          a = 2mn, b = m2 − n2, c = m2 + n2 , m > n
'''

import time

def part(stop,step):
    p=step
    p3=[]
    while True:
        for n in range(p-(p-1),p):
            for m in range(n+1,p+1):
                p3=[2*n*m,(m**2)-(n**2),(m**2)+(n**2)]
                if sum(p3)==stop:
                    return p3[0]*p3[1]*p3[2]
        p+=step

start = time.process_time()

print(part(1000,100))

print('time %f' %(time.process_time() - start))
