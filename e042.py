#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
n-ый член последовательности треугольных чисел задается как tn = ½n(n+1). 
Таким образом, первые десять треугольных чисел:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

Преобразовывая каждую букву в число, соответствующее ее порядковому номеру в алфавите, 
и складывая эти значения, мы получим числовое значение слова. 
Для примера, числовое значение слова SKY равно 19 + 11 + 25 = 55 = t10. 
Если числовое значение слова является треугольным числом, то мы назовем это слово треугольным словом.

Используя words.txt (щелкнуть правой кнопкой мыши и выбрать 'Save Link/Target As...'), 
16 КБ текстовый файл, содержащий около двух тысяч часто используемых английских слов, 
определите, сколько в нем треугольных слов.


'''

import time
from string import ascii_uppercase as alphabet
    

start = time.process_time()

def is_3(n):
    tmp = int(((8*n+1)**(1/2)-1)/2)
    return tmp**2 == (((8*n+1)**(1/2)-1)/2)**2

char2num = {letter: alphabet.index(letter) + 1 for letter in alphabet}
data=open("p042_words.txt",'r')
words=sorted(data.read().replace('"','').split(','),key=str)
count = 0
for i, val in enumerate(words):
    temp = 0
    for x in val:
        temp += char2num[x]
    if is_3(temp):
        count += 1
    
print(count)


print('time %f' %(time.process_time() - start))

