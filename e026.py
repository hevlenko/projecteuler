#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Единичная дробь имеет 1 в числителе. Десятичные представления единичных дробей со знаменателями от 2 до 10 даны ниже:

1/2 =   0.5
1/3 =   0.(3)
1/4 =   0.25
1/5 =   0.2
1/6 =   0.1(6)
1/7 =   0.(142857)
1/8 =   0.125
1/9 =   0.(1)
1/10    =   0.1
Где 0.1(6) значит 0.166666..., и имеет повторяющуюся последовательность из одной цифры. Заметим, что 1/7 имеет повторяющуюся последовательность из 6 цифр.

Найдите значение d < 1000, для которого 1/d в десятичном виде содержит самую длинную повторяющуюся последовательность цифр.
'''

import time
# import math

start = time.process_time()

def cycle_length(den):
    rest = 10
    i = 0
    while rest != 10 or i < 1:
        rest = (rest % den) * 10
        i += 1
    return i

longest = 0

for i in range(2, 1000):
    if i%2 != 0 and i%5 != 0:
        length = cycle_length(i)
        if length > longest:
            longest = length
            resultat = i

print(resultat)

print('time %f' %(time.process_time() - start))