#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Пусть d(n) определяется как сумма делителей n (числа меньше n, делящие n нацело).
Если d(a) = b и d(b) = a, где a ≠ b, то a и b называются дружественной парой, 
а каждое из чисел a и b - дружественным числом.

Например, делителями числа 220 являются 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 и 110, 
поэтому d(220) = 284. Делители 284 - 1, 2, 4, 71, 142, поэтому d(284) = 220.

Подсчитайте сумму всех дружественных чисел меньше 10000.
'''

import time
import math

start = time.process_time()

def d_n(n):
    count = 0
    for i in range(1, math.ceil(n/2) + 1):
        if n % i == 0:
            count += i
    return count

count = 0
for i in range(1, 10001):
    if d_n(d_n(i)) == i and d_n(i) != i:
        count += i

print(count)

print('time %f' %(time.process_time() - start))