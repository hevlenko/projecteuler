#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Пятизначное число 16807=7^5 является также пятой степенью натурального числа. 
Аналогично, девятизначное число 134217728 = 8^9 является девятой степенью.

Сколько существует положительных n-значных целых чисел, 
являющихся также и n-ыми степенями натуральных чисел?
'''

import time


start = time.process_time()

l = []
for i in range(1,10):
    j = 1
    while len(str(i**j))>=j:
        if len(str(i**j))==j:
            l.append(i**j)
        j += 1
print(len(l), l)

print('time %f' %(time.process_time() - start))