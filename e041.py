#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Будем считать n-значное число пан-цифровым, если каждая из цифр от 1 до n 
используется в нем ровно один раз. К примеру, 2143 является 4-значным пан-цифровым числом, а также простым числом.

Какое существует наибольшее n-значное пан-цифровое простое число?
'''

import time
import math


start = time.process_time()

def is_prime(n):
    if n < 2: 
        return False;
    if n % 2 == 0:             
        return n == 2
    k = 3
    while k*k <= n:
        if n % k == 0:
            return False
        k += 2
    return True

def is_pan(n):
    numbers='1234567'
    tmp = len(str(n))
    if set(numbers[0:tmp]) == set(str(n)):
        return True 
    return False

for i in range(7654321, 1, -1):
    if is_pan(i) and is_prime(i):
        print(i)
        break

print('time %f' %(time.process_time() - start))



