#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Простые делители числа 13195 - это 5, 7, 13 и 29.
Каков самый большой делитель числа 600851475143, являющийся простым числом?
'''

import time
import math

num = 600851475143
# num = 13195

def is_prime(n):
    for i in range(2, math.ceil(math.sqrt(n))+1):
        if n % i == 0:
            return False
    return True

def finder_prime(n):
    if is_prime(n):
        pass
    else:
        i = 1
        while True:
            i += 1
            if n % i == 0:
                n = int(n/i)
                i = 1
                if is_prime(n):
                    break
    return n

print(num, ' - ', is_prime(num))

start = time.process_time()

print(finder_prime(num))

print('time %f' %(time.process_time() - start))
