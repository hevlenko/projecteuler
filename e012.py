#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Последовательность треугольных чисел образуется путем сложения натуральных чисел. 
К примеру, 7-ое треугольное число равно 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. 
Первые десять треугольных чисел:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

Перечислим делители первых семи треугольных чисел:

 1: 1
 3: 1, 3
 6: 1, 2, 3, 6
10: 1, 2, 5, 10
15: 1, 3, 5, 15
21: 1, 3, 7, 21
28: 1, 2, 4, 7, 14, 28
Как мы видим, 28 - первое треугольное число, у которого более пяти делителей.

Каково первое треугольное число, у которого более пятисот делителей?
'''
import time
import math


start = time.process_time()

def is_sq(num):
    n = math.ceil((math.sqrt(num)))
    if n*n == num:
        return True
    else:
        return False

def max_triangular(divider):
    n = 2
    k = 3
    num = 3
    while n<=divider:
        n=2
        num += k
        for i in range(2, math.ceil(math.sqrt(num))):
            if num%i==0:
                n+=2
        if is_sq(num):
                n-=1
        k += 1
    return num

print(max_triangular(500))


print('time %f' %(time.process_time() - start))
