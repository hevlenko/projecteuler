#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
2^15 = 32768, сумма цифр этого числа равна 3 + 2 + 7 + 6 + 8 = 26.

Какова сумма цифр числа 2^1000?
'''

import time

start = time.process_time()

n = 2**1000
s = 0
for i in str(n):
    s += int(i)

print(s)

print('time %f' %(time.process_time() - start))
