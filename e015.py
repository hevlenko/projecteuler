#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Начиная в левом верхнем углу сетки 2×2 и имея возможность двигаться только 
вниз или вправо, существует ровно 6 маршрутов до правого нижнего угла сетки.
Сколько существует таких маршрутов в сетке 20×20?
'''
import time

start = time.process_time()

def fact(n):
    res = 1
    for i in range(1, n+1):
        res *= i
    return res

n = 20

amount = fact(2*n) / (fact(n)*fact(n))
print(int(amount))


print('time %f' %(time.process_time() - start))
