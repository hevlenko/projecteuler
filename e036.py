#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Десятичное число 585 = 10010010012 (в двоичной системе), является палиндромом по обоим основаниям.

Найдите сумму всех чисел меньше миллиона, являющихся палиндромами по основаниям 10 и 2.

(Пожалуйста, обратите внимание на то, что палиндромы не могут начинаться с нуля ни в одном из оснований).
'''

import time

start = time.process_time()

def is_polindrom_10(num):
    num_inverted = int(str(num)[::-1])
    if num == num_inverted:
        return True
    else:
        return False

def is_polindrom_2(num):
    num2 = int(str(bin(num))[2:])
    num2_inverted = int(str(num2)[::-1])
    if num2 == num2_inverted:
        return True
    else:
        return False

count = 0
i = 1
while i < 1000000:
    if is_polindrom_10(i) and is_polindrom_2(i):
        count += i
    i += 1

print(count) 

print('time %f' %(time.process_time() - start))