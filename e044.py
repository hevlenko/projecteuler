#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Пятиугольные числа вычисляются по формуле: Pn=n(3n−1)/2. Первые десять пятиугольных чисел:

1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...

Можно убедиться в том, что P4 + P7 = 22 + 70 = 92 = P8. Однако, их разность, 70 − 22 = 48, не является пятиугольным числом.

Найдите пару пятиугольных чисел Pj и Pk, 
для которых сумма и разность являются пятиугольными числами и значение D = |Pk − Pj| минимально, 
и дайте значение D в качестве ответа.
'''

import time
import math

start = time.process_time()

def p(n):
    return int(n*(3*n-1)/2)

def is5(n):
    return True if ((1+24*n)**0.5 + 1.0)%6.0 == 0 else False

notFound = True
d = 0
i = 0
while notFound:
    i += 1
    n = p(i)
    for j in range(i-1, 1, -1):
        m = p(j)
        if is5(n-m) and is5(n+m):
            d = n - m
            notFound = False
            break

print('D=', d)


print('time %f' %(time.process_time() - start))