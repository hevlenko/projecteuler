#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
В Англии валютой являются фунты стерлингов £ и пенсы p, и в обращении есть восемь монет:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) и £2 (200p).
£2 возможно составить следующим образом:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
Сколькими разными способами можно составить £2, используя любое количество монет?
'''

import time

start = time.process_time()

coins = [1, 2, 5, 10, 20, 50, 100, 200]
total = 200

def ways(s, i):
    if s < 0 or i < 0:
        return 0
    elif not (s and i):
        return 1
    return ways(s, i-1) + ways(s-coins[i], i)

print(ways(total, len(coins)-1))

print('time %f' %(time.process_time() - start))