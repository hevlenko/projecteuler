#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Гугол (10100) - гигантское число: один со ста нулями; 100100 почти невообразимо велико: один с двумястами нулями. Несмотря на их размер, сумма цифр каждого числа равна всего лишь 1.

Рассматривая натуральные числа вида ab, где a, b < 100, какая встретится максимальная сумма цифр числа?
'''

import time


start = time.process_time()

l = 0
for a in range(1, 100):
    for b in range(1, 100):
        tmp = 0
        for i in str(a**b):
            tmp += int(i)
        if tmp > l:
            l = tmp
print(l)

print('time %f' %(time.process_time() - start))