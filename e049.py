#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Арифметическая прогрессия: 1487, 4817, 8147, в которой каждый член возрастает на 3330, 
необычна в двух отношениях: 
(1) каждый из трех членов является простым числом, 
(2) все три четырехзначные числа являются перестановками друг друга.

Не существует арифметических прогрессий из трех однозначных, двухзначных и трехзначных простых чисел, 
демонстрирующих это свойство. Однако, существует еще одна четырехзначная возрастающая арифметическая прогрессия.

Какое 12-значное число образуется, если объединить три члена этой прогрессии?


'''

import time

start = time.process_time()

def is_prime(n):
    if n < 2: 
        return False;
    if n % 2 == 0:             
        return n == 2
    k = 3
    while k*k <= n:
        if n % k == 0:
            return False
        k += 2
    return True

l = []
for i in range(1000, 9999):
    if is_prime(i):
        l.append(i)

for i in range(len(l)-2):
    for j in range(i+1, len(l)-1):
        if set(str(l[j]))==set(str(l[i])):
            if (l[j]-l[i]+l[j]) in l:
                if set(str(l[j]))==set(str(l[j]-l[i]+l[j])):
                    x=str(l[i])+str(l[j])+str(l[j]-l[i]+l[j])
                    break

print(x)

print('time %f' %(time.process_time() - start))



