#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Выписав первые шесть простых чисел, получим 2, 3, 5, 7, 11 и 13. Очевидно, что 6-ое простое число - 13.

Какое число является 10001-ым простым числом?
'''


import time

finish = 10001

start = time.process_time()

prime = [2, 3]
tmp = prime[-1]
while len(prime) < finish:
    tmp += 2
    for x in prime:
        if tmp % x == 0:
            break
        else:
            if x == prime[-1]:
                prime.append(tmp)

print(prime[-1])

print('time %f' %(time.process_time() - start))

