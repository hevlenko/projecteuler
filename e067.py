#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Начиная с вершины представленного ниже треугольника и переходя к прилежащим числам 
в следующем ряду, максимальная возможная сумма пройденных чисел по пути от вершины до основания равна 23.

3
7 4
2 4 6
8 5 9 3

Т.е., 3 + 7 + 4 + 9 = 23.

Найти максимальную сумму при переходе от вершины до основания треугольника, 
представленного текстовым файлом размером 15КБ triangle.txt (щелкнуть правой 
кнопкой мыши и выбрать 'Save Link/Target As...'), в котором содержится треугольник с одной сотней строк.
ПРИМЕЧАНИЕ: Это намного усложненная версия 18-й задачи. Данную задачу нельзя решить, 
испробовав каждый возможный вариант, поскольку всего их 299! Если бы удалось 
проверять один триллион (1012) вариантов в секунду, потребовалось бы двадцать 
биллионов лет, чтобы испробовать их все. Существует эффективный алгоритм решения данной задачи. ;o)
'''

import time

def f_read(file_name='e067.txt'):
    base = []
    with open(file_name) as file:
        for line in file.readlines():
            line_x = line.split()
            base.append(line_x)
            for i, item in enumerate(base[-1]):
                base[-1][i] = int(item)
    return base

def step(base):
    while len(base) > 1:
        for i, item in enumerate(base[-2]):
            if item + base[-1][i] > item + base[-1][i+1]:
                base[-2][i] += base[-1][i]
            else:
                base[-2][i] += base[-1][i+1]
        base.pop()
    return base
    

start = time.process_time()
base = f_read()

step(base)
print(base)

print('time %f' %(time.process_time() - start))
