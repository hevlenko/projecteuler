#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
145 является любопытным числом, поскольку 1! + 4! + 5! = 1 + 24 + 120 = 145.

Найдите сумму всех чисел, каждое из которых равно сумме факториалов своих цифр.

Примечание: поскольку 1! = 1 и 2! = 2 не являются суммами, учитывать их не следует.
'''

import time


start = time.process_time()

l_fact = [1, 1, 2]

for i in range(3, 10):
    l_fact.append(l_fact[-1]*i)

count = 0
l_find = []
for i in range(10, 100000):
    tmp = 0
    for x in str(i):
        tmp += l_fact[int(x)]
    if i == tmp:
        count += i
        l_find.append(i)


print(l_find)
print(count)

print('time %f' %(time.process_time() - start))