#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Простое число 41 можно записать в виде суммы шести последовательных простых чисел:

41 = 2 + 3 + 5 + 7 + 11 + 13
Это - самая длинная сумма последовательных простых чисел, в результате которой получается простое число меньше одной сотни.

Самая длинная сумма последовательных простых чисел, в результате которой 
получается простое число меньше одной тысячи, содержит 21 слагаемое и равна 953.

Какое из простых чисел меньше одного миллиона можно записать в виде суммы наибольшего количества последовательных простых чисел?
'''

import time
import math

start = time.process_time()

def is_prime(n, l):
    for i in range(2, math.ceil(math.sqrt(l[-1]))+1):
        if n % i == 0:
            return False
    return True

prime = [2, 3]
N = 1000000
tmp = prime[-1]
no_stop = True
while no_stop:
    tmp += 2
    if tmp < N:
        if is_prime(tmp, prime):
            prime.append(tmp)
    else:
        no_stop = False

x = []
for i in range(len(prime)):
    for j in range(i, len(prime)):
        tmp = sum(prime[i:j])
        if tmp>N:
            break
        tmp_len = len(prime[i:j])
        if tmp_len<len(x):
            break    
        if tmp in prime:
            # if tmp_len>=len(x):
            x = prime[i:j].copy()
print(sum(x), ':', len(x), ':', x)


print('time %f' %(time.process_time() - start))