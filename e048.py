#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Сумма 11 + 22 + 33 + ... + 1010 = 10405071317.

Найдите последние десять цифр суммы 11 + 22 + 33 + ... + 10001000.
'''

import time


start = time.process_time()

count = 0
for i in range(1, 1001):
    count += i**i
print(str(count)[-10::])

print('time %f' %(time.process_time() - start))