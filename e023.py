#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Идеальным числом называется число, у которого сумма его делителей равна самому числу. 
Например, сумма делителей числа 28 равна 1 + 2 + 4 + 7 + 14 = 28, что означает, что число 28 является идеальным числом.

Число n называется недостаточным, если сумма его делителей меньше n, и называется избыточным, если сумма его делителей больше n.

Так как число 12 является наименьшим избыточным числом (1 + 2 + 3 + 4 + 6 = 16), 
наименьшее число, которое может быть записано как сумма двух избыточных чисел, равно 24. 
Используя математический анализ, можно показать, что все целые числа больше 28123 
могут быть записаны как сумма двух избыточных чисел. 
Эта граница не может быть уменьшена дальнейшим анализом, даже несмотря на то, 
что наибольшее число, которое не может быть записано как сумма двух избыточных чисел, меньше этой границы.

Найдите сумму всех положительных чисел, которые не могут быть записаны как сумма двух избыточных чисел.
'''

import time
import math
    
def getDiv(num):
    if num == 1:
        return 1
    n = math.ceil(math.sqrt(num))
    total = 1
    divisor = 2
    while divisor < n:
        if num%divisor == 0:
            total += divisor
            total += num//divisor
        divisor += 1
    if n**2 == num:
        total += n
    return total

def isAbundant(n):
    if getDiv(n) > n:
        return True
    else:
        return False

start = time.process_time()


k = 28123+1

abundentNums = []
for x in range (0, k):
    if (isAbundant(x)):
        abundentNums.append(x)
del abundentNums[0]

sums = [0]*k
for i in range(0, len(abundentNums)):
    for j in range(i, len(abundentNums)):
            tmp = abundentNums[i] + abundentNums[j]
            if tmp < k:
                if sums[tmp] == 0:
                    sums[tmp] = tmp
            else:
                break

total = 0
for x in range(0, len(sums)):
    if sums[x] == 0:
        total += x

print(total)

print('time %f' %(time.process_time() - start))


# 4179871
