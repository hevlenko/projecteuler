#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Число 197 называется круговым простым числом, потому что все перестановки 
его цифр с конца в начало являются простыми числами: 197, 719 и 971.

Существует тринадцать таких простых чисел меньше 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79 и 97.

Сколько существует круговых простых чисел меньше миллиона?
'''

import time

def is_prime(n):
    if n < 2: 
        return False;
    if n % 2 == 0:             
        return n == 2
    k = 3
    while k*k <= n:
        if n % k == 0:
            return False
        k += 2
    return True

def o(n):
    tmp = str(n)
    for i in range(len(str(n))):
        tmp = tmp[1:] + tmp[0]
        if not is_prime(int(tmp)):
            return False
    return True

start = time.process_time()

l = [] 
count = len(l)
for i in range(2, 1000000):
    if is_prime(i) and o(i):
        l.append(i)
        count += 1

print(count, l)


print('time %f' %(time.process_time() - start))