#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Каждый следующий элемент ряда Фибоначчи получается при сложении двух предыдущих. 
Начиная с 1 и 2, первые 10 элементов будут:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

Найдите сумму всех четных элементов ряда Фибоначчи, которые не превышают четыре миллиона.
'''

import time

max = 4000000

def fib(max):
    f = [1, 2]
    while True:
        f.append(f[-1] + f[-2])
        if f[-1] >= max:
            f = f [:-1]
            break
    return f

start = time.process_time()
print(sum(i for i in fib(max) if i % 2 == 0))
print('time %f' %(time.process_time() - start))

