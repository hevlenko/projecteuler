#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Дана следующая информация (однако, вы можете проверить ее самостоятельно):

1 января 1900 года - понедельник.
В апреле, июне, сентябре и ноябре 30 дней.
В феврале 28 дней, в високосный год - 29.
В остальных месяцах по 31 дню.
Високосный год - любой год, делящийся нацело на 4, однако последний год века 
(ХХ00) является високосным в том и только том случае, если делится на 400.
Сколько воскресений выпадает на первое число месяца в двадцатом веке 
(с 1 января 1901 года до 31 декабря 2000 года)?
'''

import time

start = time.process_time()

def counter(start, stop, days_passed):
    count = 0
    for i in range (start, stop+1):
        for j in range(1, 13):
            if j in [4, 6, 9, 11]:
                days_in_month = 30
            elif j == 2:
                if (i % 400) or (i % 4 == 0 and i % 100 != 0):
                    days_in_month = 29
                else:
                    days_in_month = 28
            else:
                days_in_month = 31
            if days_passed % 7 == 0:
                count += 1
            days_passed += days_in_month
    return count

print(counter(1900, 2000, 1) - counter(1900, 1900, 1))

print('time %f' %(time.process_time() - start))