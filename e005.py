#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
2520 - самое маленькое число, которое делится без остатка на все числа от 1 до 10.

Какое самое маленькое число делится нацело на все числа от 1 до 20?
'''

import time

start = time.process_time()

numbers = [i for i in range(1, 21)]

print(numbers)

for i in range(1, len(numbers)):
    for j in range(i+1, len(numbers)):
        if numbers[j] % numbers[i] == 0:
            numbers[j] = int(numbers[j] / numbers[i])

print(numbers)

result = 1
for i in range(1, len(numbers)):
    result *= numbers[i]
print(result)

print('time %f' %(time.process_time() - start))
